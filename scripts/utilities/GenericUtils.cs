/*
Created by Michael Garthforth, https://bitbucket.org/michael_garforth
*/
using UnityEngine;


static public class GenericUtils
{


    static public T Instantiate<T>(T original, Vector3 position, Quaternion rotation) where T : UnityEngine.Object
    {
        return (T)UnityEngine.Object.Instantiate(original, position, rotation);
    }


    static public T Instantiate<T>(T original) where T : UnityEngine.Object
    {
        return (T)UnityEngine.Object.Instantiate(original);
    }


}
