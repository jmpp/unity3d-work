/*
Created by Michael Garthforth, https://bitbucket.org/michael_garforth
*/
using UnityEngine;


public static class RigidbodyExtensions
{


    public static void Translate(this Rigidbody rigidbody, Vector3 offset)
    {
        Translate(rigidbody, offset, Space.Self);
    }


    public static void Translate(this Rigidbody rigidbody, Vector3 offset, Space space)
    {
        if (space == Space.Self)
        {
            offset = rigidbody.rotation*offset;
        }
        rigidbody.position += offset;
    }


}
